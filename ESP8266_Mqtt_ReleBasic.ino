/*
  Basic ESP8266 MQTT Rele example

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>//V6.03
#include <JeVe_EasyOTA.h> //EasyOta

#define releOn 0
#define releOff 1
#define relePin D5//D5=GPIO14
#define BUILTIN_LED 2 //ledBuiltin lolin Nodemcu v3

// establecemos los parametros para tener acceso a la red
const char* ssid = "vereda-R";
const char* password = "vereda2019";
const char* mqtt_server = "192.168.1.100";
const int mqtt_port= 1883;
const char* inTopic = "campo/chalet/foco";
const char* outTopic = "campo/chalet/foco";
const char* nameNode = "NodoReleChalet";
///OTA
#define ARDUINO_HOSTNAME "otaEspBasiReleChalet"
EasyOTA OTA(ARDUINO_HOSTNAME);
///ESP
WiFiClient espClient;
PubSubClient client(espClient);



void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(relePin,OUTPUT);
  digitalWrite(relePin, releOff);
  digitalWrite(BUILTIN_LED, releOff);
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  ////OTA
  OTA.onMessage([](char *message, int line) {
    Serial.println(message);
  });
  OTA.addAP(ssid, password);
   
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message recibido [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  //JSON
  StaticJsonDocument<200> doc;
  DeserializationError error = deserializeJson(doc, payload);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }
  
 
  String power = doc["power"];
  
  Serial.print("power: ");Serial.println(power);
  // Switch on the LED if an 1 was received as first character
  if (power == "on") {
    digitalWrite(BUILTIN_LED, releOn);   // Turn the LED on (Note that LOW is the voltage level
    digitalWrite(relePin, releOn);   // Turn the LED on (Note that LOW is the voltage level
    Serial.println("RELE ON");
    // it is acive low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, releOff);  // Turn the LED off by making the voltage HIGH
    digitalWrite(relePin, releOff);
    Serial.println("RELE OFF");
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(nameNode)) {
      Serial.println("connected");
      // Enviamos los datos obtenidos
     client.publish(inTopic, nameNode);
      // ... and resubscribe
      client.subscribe(inTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {
  if(!espClient.connected()){setup_wifi();}
  if (!client.connected()) { reconnect(); }
  OTA.loop();
  client.loop();

}
